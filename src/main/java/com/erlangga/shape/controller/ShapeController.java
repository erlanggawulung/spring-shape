package com.erlangga.shape.controller;

import com.erlangga.shape.repository.ShapeRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/shape")
public class ShapeController {
    @Autowired
    ShapeRepository shapeRepository;

    @GetMapping(value = "categories", produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity getCategories() {
        JSONArray categories = new JSONArray();
        categories.put("square");
        categories.put("rectangle");
        categories.put("circle");

        JSONObject response = new JSONObject();
        response.put("categories", categories);

        return ResponseEntity.ok(response.toString());
    }
}
