package com.erlangga.shape.model;

import javax.persistence.*;

@Entity
@Table(name = "shape")
public class Shape {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String category;
    private Float area;
}
