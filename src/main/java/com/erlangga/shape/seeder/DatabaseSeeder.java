package com.erlangga.shape.seeder;

import com.erlangga.shape.model.Role;
import com.erlangga.shape.model.User;
import com.erlangga.shape.repository.RoleRepository;
import com.erlangga.shape.repository.UserRepository;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class DatabaseSeeder {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(DatabaseSeeder.class);

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seedRoleTable();
        seedUsersTable();
    }

    private void seedRoleTable() {
        String role0 = "admin", role1 = "user";
        String sql = "SELECT name FROM role r WHERE r.name IN (\'" + role0 + "\', \'" + role1 + "\')";
        List<Role> rs = jdbcTemplate.query(sql, (resultSet, rowNum) -> null);
        if(rs == null || rs.size() <= 0) {
            Role r0 = new Role(role0);
            Role r1 = new Role(role1);
            roleRepository.save(r0);
            roleRepository.save(r1);
            logger.info("role table seeded");
        }else {
            logger.trace("role Seeding Not Required");
        }
    }

    private void seedUsersTable() {
        String sql = "SELECT username FROM \"user\" U WHERE U.username = \'admin@gmail.com\' LIMIT 1";
        List<User> u = jdbcTemplate.query(sql, (resultSet, rowNum) -> null);
        if(u == null || u.size() <= 0) {
            User user = new User();
            user.setUsername("admin@gmail.com");
            user.setPassword(new BCryptPasswordEncoder().encode("qweasdzxc"));
            Set<Role> roles = new HashSet<>();
            Role admin = roleRepository.findByName("admin");
            roles.add(admin);
            user.setRoles(roles);
            userRepository.save(user);
            logger.info("Users Seeded");
        } else {
            logger.trace("Users Seeding Not Required");
        }
    }
}
