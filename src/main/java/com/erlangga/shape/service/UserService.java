package com.erlangga.shape.service;

import com.erlangga.shape.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
